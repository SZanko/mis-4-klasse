#!/bin/sh

#Installieren der Software
sudo pacman -S fuse-overlayfs podman podman-docker \
	       buildah neovim bind-tools curl

#Basic Vim Setup
mkdir -p ~/.config/nvim
echo "set number" >> ~/.config/nvim/init.vim

#Rootless Mode Konfigurieren 
sudo echo "${USER}:100000:65536" >> /etc/subuid
sudo echo "users:100000:65536" >> /etc/subgid

echo "Ausloggen und wieder Anmelden oder in eine neue Gruppe einloggen"
